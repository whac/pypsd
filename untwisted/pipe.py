import os
import logging
import fcntl

class Pipe():

	_log = logging.getLogger(__name__)
	_reactor = None
	_out_buffer = " "
	events = 0

	pipe_read = None
	pipe_write = None

	def __init__(self, reactor):
		self.pipe_read, self.pipe_write = os.pipe()

		flags = fcntl.fcntl(self.pipe_read, fcntl.F_GETFL, 0)
		flags = flags | os.O_NONBLOCK
		flags = fcntl.fcntl(self.pipe_read, fcntl.F_SETFL, flags)

		flags = fcntl.fcntl(self.pipe_write, fcntl.F_GETFL, 0)
		flags = flags | os.O_NONBLOCK
		flags = fcntl.fcntl(self.pipe_write, fcntl.F_SETFL, flags)

		self._reactor = reactor
		self._reactor.engine.RegisterSocket(self)
		self._reactor.engine.RequestRead(self)
	
	def Shutdown(self):
		self._reactor.engine.UnregisterSocket(self)

		os.close(self.pipe_read)
		os.close(self.pipe_write)
	
	def ProcessRead(self):
		try:
			line = os.read(self.pipe_read, 4096)
		except:
			pass

		self.OnRead(line)
	
	def Write(self, data = "*"):
		self._out_buffer += data
		self._reactor.engine.RequestWrite(self)
	
	def ProcessWrite(self):
		if len(self._out_buffer) == 0:
			self._reactor.engine.CancelWrite(self)
			return

		sent = os.write(self.pipe_write, self._out_buffer)
		if sent == -1:
			self._log.error("Error writing to pipe: %s" % ex)
			raise Exception("Error writing to pipe: %s" % ex)

		self._out_buffer = self._out_buffer[sent:]
		if len(self._out_buffer) == 0:
			self._reactor.engine.CancelWrite(self)

	def ProcessError(self):
		self.Shutdown()
		self._log.error("Error on pipe")
	
	def OnError(self, ex):
		pass
	
	def GetFD(self):
		return self.pipe_read
	
	def OnRead(self, line):
		pass
