import select
import time
import traceback
import logging
import errno

class SelectEngine():
	_log = logging.getLogger(__name__)
	_fd_to_socket = {}
	_fds_to_read = []
	_fds_to_write = []

	def Start(self):
		pass
	
	def Stop(self):
		pass

	def RegisterSocket(self, socket):
		self._fd_to_socket[socket.GetFD()] = socket
	
	def UnregisterSocket(self, socket):
		del self._fd_to_socket[socket.GetFD()]
	
	def RequestRead(self, socket):
		self._fds_to_read.append(socket.GetFD())
	
	def CancelRead(self, socket):
		self._fds_to_read.remove(socket.GetFD())
	
	def RequestWrite(self, socket):
		self._fds_to_write.append(socket.GetFD())
	
	def CancelWrite(self, socket):
		self._fds_to_write.remove(socket.GetFD())
	
	def Poll(self):
		if (len(self._fds_to_read) == 0) and (len(self._fds_to_write) == 0):
			time.sleep(1)
			return

		try:
			ready_fds_to_read, ready_fds_to_write, null = select.select(self._fds_to_read, self._fds_to_write, [], 1)
		except IOError, ex:
			if ex.errno != errno.EINTR:
				self._log.error("Error in select.Poll: " + str(ex))
				traceback.print_exc()
				time.sleep(1)
			return

		for fd in ready_fds_to_read:
			try:
				socket = self._fd_to_socket[fd]
			except KeyError:
				continue
			
			if not socket:
				continue

			try:
				socket.ProcessRead()
			except Exception, ex:
				traceback.print_exc()
				socket.OnError(ex)
				socket.Shutdown()

		for fd in ready_fds_to_write:
			try:
				socket = self._fd_to_socket[fd]
			except KeyError:
				continue
			
			if not socket:
				continue

			try:
				socket.ProcessWrite()
			except Exception, ex:
				traceback.print_exc()
				socket.OnError(ex)
				socket.Shutdown()
