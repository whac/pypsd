# Try our 'best' engines first
try:
	from select import epoll
	from .epoll_engine import EpollEngine
	recommended_engine = EpollEngine

# Fall back to select.select , which works everywhere
except ImportError:
	from .select_engine import SelectEngine
	recommended_engine = SelectEngine
