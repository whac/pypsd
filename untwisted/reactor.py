import select
import logging
import task
import signal
from pipe import Pipe
import os

class SignalPipe(Pipe):
	def __init__(self, reactor):
		Pipe.__init__(self, reactor)

	def OnRead(self, line):
		try:
			while os.waitpid(0, os.WNOHANG) != (0, 0):
				pass
		except:
			pass

class Reactor():
	running = False
	signal_pipe = None

	_log = logging.getLogger(__name__)
	engine = None

	def Init(self):
		import untwisted.engines
		self.engine = untwisted.engines.recommended_engine()
		self.engine.Start()

		self.signal_pipe = SignalPipe(self)
		signal.set_wakeup_fd(self.signal_pipe.pipe_write)
		signal.signal(signal.SIGCHLD, lambda x, y: None)
	
	def Stop(self):
		if self.running:
			signal.set_wakeup_fd(1)
			self.signal_pipe.Shutdown()
			self.running = False
		else:
			self.engine.Stop()

	def Start(self):
		self.running = True
		while self.running:
			self.engine.Poll()
			task.Run()
		self.Stop()

