#!/usr/bin/env python2
# A IRCD PseudoServer, written for Rizon
# (c) Nick Pappas (radicand) http://www.radicand.org/

import sys
import os
import platform
import time
import ConfigParser
import codecs
import logging
import logging.handlers
import traceback

from untwisted.ircsocket import IRCSocket
from untwisted.reactor import Reactor
from untwisted import task

IRCF_COLOR = chr(3)

config = ConfigParser.ConfigParser()
config.readfp(codecs.open("config.ini", "r", "utf8"))

# just temporarily, so we don't crash production without letting people know what changed
if not config.has_option('server', 'whois_faked_server'):
	print "We now require a 'whois_faked_server' option under config.ini's 'server' section."
	print "To get it working just like before, add the following line under config.ini's 'server' section:"
	print "     whois_faked_server: *.rizon.net"
	print "Look at the 'server' section in config.ini.sample for more details."
	sys.exit(0)

#GLOBALS
L_IN = 1
L_OUT = 2
L_STATUS = 4
L_SECURITY = 8
L_DEBUG = 16
L_SQL = 32
L_COMMAND = 64
L_ERROR = 128

reactor = None
protocol_module = None

class PyPSDIRCSocket(IRCSocket):
	addr = None
	port = None

	def OnConnectError(self, errno):
		self._log.error("Connection error #" + str(errno))

	def OnConnectStarted(self, addr, port):
		self.addr = addr
		self.port = port
		self._log.info("Started to connect to uplink %s:%d" % (self.addr, self.port))
	
	def OnConnectSuccess(self):
		self._log.info("Successfully connected to uplink %s:%d" % (self.addr, self.port))
		protocol_module.connectionMade()
	
	def OnError(self, ex):
		self._log.error("Lost connection. Reason: %s" % ex)
		protocol_module.connectionLost(str(ex))

		#if not protocol_module.shutting_down:
		#	self._log.info("Will reconnect in 30 seconds")
		#	t = task.LoopingCall(connect)
		#	t.start(30, False)
		#else:
		reactor.Stop()

	def OnRead(self, line):
		self._log.debug(" <- " + line)

		tokens = line.split(" ")
		if len(tokens) < 2:
			return

		source = ""
		if tokens[0][0] == ':':
			source = tokens[0][1:]
			tokens.pop(0)

		command = tokens.pop(0)

		for i in range(len(tokens)):
			if len(tokens[i]) > 0 and tokens[i][0] == ':':
				tokens[i] = " ".join(tokens[i:])[1:]
				while i + 1 < len(tokens):
					tokens.pop(i + 1)
				break

		self._log.debug("SOURCE: %s, COMMAND: %s, PARAMETERS: %s" % (source, command, tokens))
		global protocol_module
		try:
			func = getattr(protocol_module, "irc_" + command)
		except AttributeError:
			return

		try:
			func(source, tokens)
		except:
			traceback.print_exc()

def connect():
	global reactor
	try:
		uplink_socket = PyPSDIRCSocket(reactor)
		protocol_module.socket = uplink_socket
		uplink_socket.Bind(config.get('uplink', 'bind'))
		uplink_socket.Connect(config.get('uplink', 'ip'), config.getint('uplink', 'port'))
	except Exception, err:
		log.error("Error, %s" % err)
		sys.exit(1)

#core startup functionality
def main():
	global protocol_module

	try:
		prototype = config.get('server', 'protocol')
		proto = __import__("protocol." + prototype.lower())
		protocol_module = eval("proto." + prototype.lower() + "." + prototype)()
	except Exception, err:
		log.critical("Error loading protocol '%s'. (%s)" % (prototype, err))
		sys.exit(1)
	
	# Load modules
	modules = config.get('modules', 'autoload')
	if modules.isspace() or len(modules) == 0: mlist = []
	else: mlist = modules.split(",")
	try:
		map(protocol_module.loadModule, mlist)
	except Exception, err:
		log.error("Error loading modules (%s)" % err)
		sys.exit(1)
	
	global reactor
	reactor = Reactor()
	reactor.Init()

	connect()

	reactor.Start()

if __name__ == "__main__":
	#init logger
	logfile = config.get('logging', 'logfile')
	loglevel = getattr(logging, config.get('logging', 'level').upper())
	
	FORMAT = '%(asctime)s %(name)s(%(lineno)s) [%(levelname)s] %(message)s'
	handler = logging.handlers.TimedRotatingFileHandler(
    	logfile, when='midnight', backupCount=7)
	handler.setFormatter(logging.Formatter(FORMAT))
	
	log = logging.getLogger('')
	log.addHandler(handler)
	log.setLevel(loglevel)
	
	if config.getint('control', 'debug') == 1:
		main()
	else:
		if platform.system() == "Linux":
			# TODO: Switch this to check if its not Windows instead.
			# FBSD, etc have this functionality as well. -rad
			
			# Windows doesn't have the .fork() system call, so only run this on windows
			try:
				pid = os.fork()
				if pid > 0: sys.exit(0)
			except OSError, err:
				log.critical("Error #%d encountered while trying to fork: %s" % (err.errno, err.strerror))
				sys.exit(1)
			try:
				pid = os.fork()
				if pid > 0:
					log.info("Daemonizing into PID %d" % pid)
					sys.exit(0)
			except OSError, err:
				log.critical("Error #%d encountered while trying to do second fork: %s" % (err.errno, err.strerror))
				sys.exit(1)
			main()
		else:
			main()
	
