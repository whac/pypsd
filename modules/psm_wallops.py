#!/usr/bin/python pseudoserver.py
#psm-acl.py
# module for pypseudoserver
# written by ???
# redirects wallops to a channel

from psmodule import *
from untwisted.istring import istring

class PSModule_wallops(PSModule):
	VERSION = 1
	
	uid = ""

	def startup(self):
		if not PSModule.startup(self): return False
		self.log = logging.getLogger(__name__)
		
		try:
			self.uid, user = self.parent.createFakeUser(
				self.config.get('wallops', 'nick'),
				self.config.get('wallops', 'user'),
				self.config.get('wallops', 'host'),
				self.config.get('wallops', 'gecos'),
				self.config.get('wallops', 'modes'),
				self.config.get('wallops', 'nspass'),
				version="PyPsd WALLOPS v%d" % self.VERSION
			)
			
			self.reload()
			
			return True
			
		except Exception, err:
			self.log.error("Error creating user for wallops, is the config correct? (%s)" % err)
			return False
		
	def shutdown(self):
		PSModule.shutdown(self)
		self.parent.quitFakeUser(self.uid)
		
	def reload(self):
		try:
			PSModule.reload(self)
			self.chan = istring(self.config.get('wallops', 'channel'))
			
			try:
				self.parent.sendMessage("PART", self.chan, prefix = self.uid)
				self.parent.part_channel(self.uid, chan)
			except Exception, err:
				pass
				
			self.parent.sendMessage("JOIN", str(self.parent.mkts()), self.chan, '+', prefix = self.uid)
			self.parent.join_channel(self.uid, chan)
					
			return True
			
		except Exception, err:
			self.log.warning('Trying to part channel that module is not joined, this should be safe')
			
			return True
		
	def operwall(self, prefix, params):
		try:
			foo, userinfo = self.parent.get_user(prefix)
			user = userinfo['nick']
		except Exception, e:
			user = prefix

		self.parent.privMsg(self.chan, '%s: %s' % (user,params[0]), self.uid)
		
	def cmd_reload(self, source, target, pieces):
		if self.reload():
			self.parent.privMsg(self.logchan, 'Channels reloaded.', self.uid)
		else:
			self.parent.privMsg(self.logchan, 'Error while reloading channels', self.uid)
			
		return True
		
	def getCommands(self):
		return (
			('wrreload', {
			'permission':'r',
			'callback':self.cmd_reload,
			'usage':"reloads wallops channel(s)"})
		)
			
	def getHooks(self):
		return (
			('OPERWALL', self.operwall),
			('WALLOPS', self.operwall),
		)
