from datetime import datetime
from feed import XmlFeed
from ..utils import unescape
from xml.dom import minidom
import urllib

class Quotes(object):
	def __init__(self, key_fml):
		self.key_fml = key_fml
		self.fml_cache = {} # {'quotes': xml-elements, 'next': next quote index} // 10-quotes cache
		self.qdb_cache = {} # {'quotes': xml-elements, 'ts': datetime-timestamp, 'next': next quote index} // page cached for 15s
	
	def get_qdb_random(self):
		if not self.qdb_cache or (datetime.now() - self.qdb_cache['ts']).seconds > 15:
			qdb = XmlFeed('http://qdb.us/qdb.xml?action=random&fixed=0')
			quotes = qdb.elements('//item')
			next = 0
			self.qdb_cache = {'quotes': quotes, 'ts': datetime.now(), 'next': next}
		else:
			quotes = self.qdb_cache['quotes']
			next = self.qdb_cache['next']
		
		quote = quotes[next]
		self.qdb_cache['next'] = next + 1
		lines = unescape(quote.text('description')).split('<br />')
		if len(lines) > 5:
			return self.get_qdb_random()
		else:
			return {'lines': lines, 'link': quote.text('link'), 'id': quote.text('title')}
	
	def get_qdb_id(self, quote_id):
		qdb = XmlFeed('http://qdb.us/qdb.xml?action=quote&quote=%d&fixed=0' % quote_id)
		quotes = qdb.elements('//item')
		if quotes:
			quote = quotes[0]
			lines = unescape(quote.text('description')).split('<br />')
			if len(lines) > 5:
				return {'lines': ['this quote is too long. You can view it here: %s' % quote.text('link')], 'link': quote.text('link'), 'id': quote.text('title')}
			else:
				return {'lines': lines, 'link': quote.text('link'), 'id': quote.text('title')}
		else:
			return None
		
	def get_fml_random(self):
		if not self.fml_cache or self.fml_cache['next'] == 10:
			feed = minidom.parse(urllib.urlopen('http://api.fmylife.com/view/random/10/nocomment?key=%s&language=en' % self.key_fml))
			quotes = feed.getElementsByTagName('item')
			if len(quotes) == 0:
				raise FmlException("fmylife.com is temporarily unavailable. Please try again later.")
			
			next = 0
			self.fml_cache['quotes'] = quotes
			self.fml_cache['next'] = next
		else:
			quotes = self.fml_cache['quotes']
			next = self.fml_cache['next']
		
		quote = quotes[next]
		self.fml_cache['next'] = next + 1

		text = quote.getElementsByTagName('text')[0].toxml()[6:-7]
		id = quote.getAttribute('id')
		category = quote.getElementsByTagName('category')[0].toxml()[10:-11]

		quote.unlink()

		return {'text': text, 'id': id, 'category': category}
	
	def get_fml_id(self, quote_id):
		feed = XmlFeed('http://api.fmylife.com/view/%d/nocomment?key=%s&language=en' % (quote_id, self.key_fml))
		quotes = feed.elements('/root/items/item')
		if quotes:
			quote = quotes[0]
			return {'text': quote.text("*[starts-with(local-name(), 'tex')]"), 'id': quote.text('@id'), 'category': quote.text('category')}
		else:
			return None

class FmlException(Exception):
	def __init__(self, e):
		self.msg = e
	
	def __str__(self):
		return self.msg
