#!/usr/bin/python pseudoserver.py
# psm_registration.py
# module for pypseudoserver
# written by B (ben@rizon.net)
# helpful registration bot!

from psmodule import *

class PSModule_registration(PSModule):
	VERSION = 1
		
	def startup(self):
		if not PSModule.startup(self): return False
		self.log = logging.getLogger(__name__)
				
		try:
			self.uid, user = self.parent.createFakeUser(
				self.config.get('registration', 'nick'),
				self.config.get('registration', 'user'),
				self.config.get('registration', 'host'),
				self.config.get('registration', 'gecos'),
				self.config.get('registration', 'modes'),
				self.config.get('registration', 'nspass'),
				version="PyPsd Registration v%d" % self.VERSION
			)
		except Exception, err:
			self.log.exception("Error creating registration module user: %s" % err)
			raise
	
		return True
		
	def shutdown(self):
		PSModule.shutdown(self)
		self.parent.quitFakeUser(self.uid)

## Begin event hooks

	def registration_TMODE(self, prefix, params):
		"""Picks up the +z -z modes on set by Anope Services on registration and dropping of channels
		2009-07-10 00:42:42+0000 [TS6Protocol,client] [TMODE](02SAAAAAD): 1245351951|#random99|+z
		2009-07-10 00:42:34+0000 [TS6Protocol,client] [TMODE](02SAAAAAD): 1245351951|#random99|-z
		"""
		chan = params[1];
		mode = params[2];
		if mode != "+z": return #ignore all other mode changes, we only want +z for starts
		self.log.debug("Caught a channel registration")
		
		# taken from ts6protocol.py
		# join
		now = self.parent.mkts()
 		self.parent.sendMessage("JOIN", "%d %s %s" % (now, chan, "+"), prefix=self.uid)
		self.parent.join_channel(self.uid, chan)
		#self.parent.privMsg(self.logchan, "Joined %s" % chan, self.uid)
		
		
		#spam!
		self.parent.privMsg(chan, self.config.get('registration', 'reg1'), self.uid)
		self.parent.privMsg(chan, self.config.get('registration', 'reg2'), self.uid)
		self.parent.privMsg(chan, self.config.get('registration', 'reg3'), self.uid)
		self.parent.privMsg(chan, self.config.get('registration', 'reg4'), self.uid)
		self.parent.privMsg(chan, self.config.get('registration', 'reg5'), self.uid)
		self.parent.privMsg(chan, self.config.get('registration', 'reg6'), self.uid)

		#logging fun
		self.parent.privMsg(self.logchan, "registration info sent to %s" % chan, self.uid)
		
		#flee!
		self.parent.sendMessage("PART", chan, prefix=self.uid)
		self.parent.part_channel(self.uid, chan)
		#self.parent.privMsg(self.logchan, "Left %s" % chan, self.uid)
		
		
	def getHooks(self):
		return (('tmode', self.registration_TMODE),)
