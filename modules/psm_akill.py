#!/usr/bin/python pseudoserver.py
#psm_akill.py
# module for pypseudoserver
# written by Zion
# kline monitoring module

from psmodule import *

import re
import time
import socket
import calendar
import random
import string

from untwisted import task

class PSModule_akill(PSModule):
	VERSION = 2
	VERSION_MINOR = 0
	
	class akill(object):
		id = None
		ip = 0
		source = None
		comment = None
		code = 0
		expire = 0
		user = None
	
	akills = {}
	uid = ""
		
	def __init__(self, parent, config):
		PSModule.__init__(self, parent, config)
		self.log = logging.getLogger(__name__)
		
		try:
			self.dbp.execute("CREATE TABLE IF NOT EXISTS akill (id VARCHAR(12), \
			ip VARCHAR(100) PRIMARY KEY,source VARCHAR(100), comment VARCHAR(255), code INT(3), \
			expire INT(13), user VARCHAR(100), UNIQUE KEY(ip));")
		except Exception, err:
			self.log.exception("Error creating table for akill module (%s)" % err)
		
		try:
			self.updatetimer = self.config.getint('akill','updatetimer')
			self.loglvl = self.config.getint('akill','loglvl')
		except Exception, err:
			self.log.exception("Error while loading configuration, does it exists? (%s)" % err)

			
	def startup(self):
		if not PSModule.startup(self): return False
		
		try:
			self.uid, user = self.parent.createFakeUser(
				self.config.get('akill', 'nick'),
				self.config.get('akill', 'user'),
				self.config.get('akill', 'host'),
				self.config.get('akill', 'gecos'),
				self.config.get('akill', 'modes'),
				self.config.get('akill', 'nspass'),
				version="PyPsd Akill manager v%d" % self.VERSION
			)
		except Exception, err:
			self.log.exception("Error creating user for akill, is the config correct? (%s)" % err)
			raise
		
		try:
			self.dbp.execute("SELECT * FROM akill")
			for row in self.dbp.fetchall():
				new_akill = self.akill()
				new_akill.id = row[0]
				new_akill.ip = self.get_ip(row[1])
				new_akill.source = row[2]
				new_akill.comment = row[3]
				new_akill.code = int(row[4])
				new_akill.expire = int(row[5])
				new_akill.user = row[6]
				
				self.akills[new_akill.ip] = new_akill
		except Exception, err:
			self.log.exception("Error while updating from akill (%s)" % err)
			raise
			
		self.parent.sendMessage("STATS", 'k', prefix=self.uid)
		self.parent.sendMessage("STATS", 'K', prefix=self.uid)

		self.parent.privMsg(self.logchan, "AKILL: v%s.%s loading, holding %s records" % 
			(self.VERSION, self.VERSION_MINOR, len(self.akills)), self.uid)
		
		try:	
			self.db_update = task.LoopingCall(self.db_update)
			self.db_update.start(self.updatetimer)
		except Exception, err:
			self.log.exception("Error while running looping calls (%s)" % err)
			raise
		
		return True
		
	def shutdown(self):
		PSModule.shutdown(self)
		
		self.db_update.stop()
		
		self.parent.quitFakeUser(self.uid)
		
	def getVersion(self):
		return self.VERSION
	
	def db_update(self):
		try:
			stat = self.dbp.execute("DELETE FROM akill WHERE expire != 0 and expire < UNIX_TIMESTAMP();")
		except Exception, err:
			self.log.exception("Error while updating database (%s)" % err)
		
		count = 0
		now = time.time()
		
		keys = self.akills.keys()
		for ip in keys:
			akill = self.akills[ip]
			if akill.expire != 0 and akill.expire < now:
				del self.akills[ip]
				count += 1
		if count > 0:
			self.parent.privMsg(self.logchan, "AKILL: Removed " + str(count) + " expired KLines, now holding " + str(len(self.akills)) + " records", self.uid)

	def akill_219(self, prefix, params):
		if not params[1] == 'k':
			return
			
		self.parent.privMsg(self.logchan, "AKILL: Finished syncing with STATS, now holding " + str(len(self.akills)) + " records", self.uid)

	def akill_216(self, prefix, params):
		if not len(params) == 6:
			return
		
		source = 'Stats'
		
		added = 0
		try:
			datetime = re.findall(r"\((\d+\/\d+\/\d+\s\d+\.\d+)\)", params[5])
			added = calendar.timegm(time.strptime(datetime[0], "%Y/%m/%d %H.%M"))
		except Exception, err:
			self.log.exception("Error while parsing added time from kstats (%s)" % err)
			return
		
		expires = 0
		try:
			exp_in = re.findall(r"\s(\d+)\smin", params[5])
			if exp_in:
				expires = int(added) + (int(exp_in[0]) * 60)
		except Exception, err:
			self.log.exception("Error while parsing added time from kstats (%s)" % err)
			return
		
		ip = self.get_ip(params[2])
		if not ip:
			return
		try:
			current_akill = self.akills[ip]
			source = current_akill.source
		except:
			pass
			
		comment = self.shared_comment(params[5])
		
		code = self.shared_code(params[5])
		if not code:
			return
			
		user = self.shared_user(params[5])
		id = self.shared_id(params[5])
		
		try:
			self.dbp.execute("REPLACE INTO akill (id, ip, source, comment, code, expire, user) VALUES (%s,%s,%s,%s,%s,%s,%s);",
				(id, ip, source, comment, int(code), expires, user))
		except Exception, err:
			self.log.exception("Error while saving ip from kstats (%s)" % err)
		
		new_akill = self.akill()
		new_akill.id = id
		new_akill.ip = ip
		new_akill.source = source
		new_akill.comment = comment
		new_akill.code = int(code)
		new_akill.expire = int(expires)
		new_akill.user = user
		
		self.akills[ip] = new_akill
	
	def akill_kline(self, prefix, params):
		try:
			foo, userinfo = self.parent.get_user(prefix)
			source = userinfo['nick']
		except Exception, e: source = prefix
		
		expires = int(params[1])
		if expires < 0:
			expires = 0
		else:
			expires += time.time()
		
		ip = self.get_ip(params[3])
		
		comment = params[4]
		
		code = self.shared_code(params[4])
		if not code:
			return
			
		user = self.shared_user(params[4])
		id = self.shared_id(params[4])
		
		try:
			result = self.dbp.execute("REPLACE INTO akill (id, ip, source, comment, code, expire, user) VALUES (%s,%s,%s,%s,%s,%s,%s);",
				(id, ip, source, comment, int(code), expires, user))
		except Exception, err:
			self.log.exception("Error while saving ip from kline (%s)" % err)

		new_akill = self.akill()
		new_akill.id = id
		new_akill.ip = ip
		new_akill.source = source
		new_akill.comment = comment
		new_akill.code = int(code)
		new_akill.expire = int(expires)
		new_akill.user = user
		
		self.akills[ip] = new_akill

	def akill_unkline(self, prefix, params):
		ip = self.get_ip(params[2])
		
		try:
			stat = self.dbp.execute("DELETE FROM akill WHERE ip = '%s';" % ip)
		except Exception, err:
			self.log.exception("Error while removing record via unkline (%s)" % err)
		
		del self.akills[ip]

	def get_ip(self, ip):
		try:
			hosts = socket.getaddrinfo(ip, None)
			return hosts[0][4][0]
		except socket.gaierror:
			pass
		
		return ip

	def shared_comment(self, param):
		try:
			return re.findall(r"(\[.+)\s\(", param)[0]
		except Exception:
			pass
		
		try:
			return re.findall(r"(.+)\s\(", param)[0];
		except Exception:
			pass
		
		return param

	def shared_code(self, param):
		try:
			code = re.findall("Code\s[\#]*(\d{1,4})", param)
			if not code:
				return '0'
			else:
				return code[0]
		except Exception:
			self.log.exception("Error while parsing code from: %s" % param)
			return None

	def shared_user(self, param):
		user = re.findall(r"\[([A-Za-z\-]+)\]", param)
		if user:
			if len(user) >= 2:
				return user[1]
			else:
				return user[0]
		else:
			return 'none'
	
	def shared_id(self, param):
		id = re.findall("Akill ID: ([A-Za-z0-9]+)", param)
		if id:
			return id[0]
		return "None"

	def get_time(self, timestamp):
		if timestamp == 0:
			return "Never"

		exp = int(timestamp) - time.time()
		
		if exp < 0:
			return "Next database update"
		
		days = hours = minutes = 0

		while exp > 86400:
			days += 1
			exp -= 86400
		while exp > 3600:
			hours += 1
			exp -= 3600
		while exp > 60:
			minutes += 1
			exp -= 60
		
		if days > 0:
			return "%d days, %d hours and %d minutes" % (days, hours, minutes)
		elif hours > 0:
			return "%d hours and %d minutes" % (hours, minutes)
		else:
			return "%d minutes and %d seconds" % (minutes, exp)
	
	def reply(self, source, target, message):
		if source == target:
			self.parent.notice(target, message, self.uid)
		else:
			self.parent.privMsg(target, message, self.uid)

	def cmd_search(self, source, target, pieces):
		if not pieces or not len(pieces) >= 1:
			return False
		
		count = 0
		try:
			limit = int(pieces[1])
		except:
			limit = 1
		
		for ip in self.akills:
			if pieces[0] in ip:
				count += 1
				if count <= limit: 
					akill = self.akills[ip]
					self.reply(source, target, "#%s: *@%s: %s... %s (#%s) - Expires in: %s" %
						(akill.id, akill.ip, akill.source, akill.comment[:60], akill.code, self.get_time(akill.expire)))

		showed = count
		if count > limit:
			showed = limit
	
		self.reply(source, target, "END OF ASEARCH (%d/%d)" % (showed, count))
		return True

	def cmd_stats(self, source, target, pieces):
		verbose = False
		if pieces and pieces[0] == 'all':
			verbose = True

		sources = {}

		codes = {}
		codes[0] = "No code information."
		codes[1] = "Regex matches to known bot patterns."
		codes[2] = "Spam traps, unsolicited DCC transfers."
		codes[3] = "TOR - currently unused."
		codes[4] = "Drones / Flood Bots."
		codes[5] = "Administrator added."
			
		code = {}
		user = {}

		for ip in self.akills:
			akill = self.akills[ip]

			if not akill.source in sources.keys():
				sources[akill.source] = 1
			else:
				sources[akill.source] += 1
					
			if not akill.code in code.keys():
				code[akill.code] = 1
			else:
				code[akill.code] += 1

			if not akill.user in user.keys():
				user[akill.user] = 1
			else:
				user[akill.user] += 1
					
		self.reply(source, target, "Current number of AKILLs: %s" % len(self.akills))	
		if verbose:
			self.reply(source, target, "Current number of sources: %s" % len(sources.keys()))
			for each in sources.keys():
				self.reply(source, target, "%s records added via %s" % (sources[each], each))
		self.reply(source, target, "Current number of users contributed: %s" % len(user.keys()))
		if verbose:
			for each in user.keys():
				self.reply(source, target, "%s added %s records" % (each, user[each]))
			for each in code.keys():
				try:
					explain = codes[int(each)]
				except Exception:
					explain = codes[0]
				self.reply(source, target, "%s records added as code #%s - %s" % (code[each], each, explain))		
		return True

	def cmd_add(self, source, target, pieces):
		if not pieces or len(pieces) < 3:
			return False
		elif pieces[1].find('@') != -1:
			self.reply(source, target, "Do not use ident@, only the IP/host")
			return True

		uid, adder = self.parent.get_user(source)
		if not adder:
			return False
		
		expires = 0
		try:
			expires = int(pieces[0][:-1])
			if pieces[0][-1] == 'd':
				expires *= 86400
			elif pieces[0][-1] == 'h':
				expires *= 3600
			elif pieces[0][-1] == 'm':
				expires *= 60
			if expires > 0:
				expires += time.time()
		except:
			self.reply(source, target, "Error parsing expiry time, defaulting to 30d")
			expires = time.time() + (30 * 86400)

		mask = pieces[1]
		uid, user = self.parent.get_user(pieces[1])
		if user:
			mask = user['ip']
			if mask == "0" or self.parent.has_umode(pieces[1], 'N'):
				self.reply(source, target, "%s is a protected user." % (user['nick']))
				return True
		else:
			mask = self.get_ip(mask)
		
		if not re.findall("[A-Za-z0-9]", mask):
			self.reply(source, target, "%s is too wide, are you trying to Kyouka-sama the network?" % (mask))
			return True
		
		reason = string.join(pieces[2:], " ")
		
		id = ""
		for i in range(1, 10):
			ch = '^'
			while ch.isalnum() == False:
				ch = chr(int(random.random() * 100))
			id += ch
		
		new_akill = self.akill()
		new_akill.id = id
		new_akill.ip = mask
		new_akill.source = self.config.get('akill', 'nick')
		new_akill.comment = "[P][" + adder['nick'] + "] " + reason + " (Akill ID: " + id + ")"
		new_akill.code = 0
		new_akill.expire = expires
		new_akill.user = adder['nick']
		
		self.akills[mask] = new_akill
		
		try:
			result = self.dbp.execute("REPLACE INTO akill (id, ip, source, comment, code, expire, user) VALUES (%s,%s,%s,%s,%s,%s,%s);",
				(new_akill.id, new_akill.ip, new_akill.source, new_akill.comment, int(new_akill.code), new_akill.expire, new_akill.user))
		except Exception, err:
			self.log.exception("Error while saving ip from cmd_add (%s)" % err)
		
		self.parent.sendMessage("KLINE", "* %d * %s :%s" % (expires, mask, new_akill.comment), prefix=self.uid)
		self.reply(source, target, "%s added to the akill list." % (mask))
		self.parent.sendMessage("OPERWALL", ":%s added an AKILL for *@%s (%s) expires in %s" % (adder['nick'], mask, new_akill.comment, self.get_time(expires)), prefix=self.uid)
		
		return True

	def cmd_del(self, source, target, pieces):
		if not pieces or len(pieces) < 1:
			return False
		
		uid, adder = self.parent.get_user(source)
		if not adder:
			return False
		
		mask = pieces[0]
		has = self.akills.has_key(mask)
		if has == False:
			for ip in self.akills:
				akill = self.akills[ip]
				if akill.id == mask:
					mask = akill.ip
					has = True
		
		if has == False:
			self.parent.privMsg(self.logchan, "%s is not akilled." % mask, self.uid)
			return True

		self.parent.sendMessage("UNKLINE", "* * %s" % (mask), prefix=self.uid)
		self.reply(source, target, "%s removed from the akill list." % (mask))
		self.parent.sendMessage("OPERWALL", ":%s removed an AKILL for *@%s" % (adder['nick'], mask), prefix=self.uid)
		
		return True
	
	def getCommands(self):
		return (
			('search', {
			'permission':'A',
			'callback':self.cmd_search,
			'usage':"<partial ip address> [results] - searches for given number or records inside akill database"}),
			('stats', {
			'permission':'A',
			'callback':self.cmd_stats,
			'usage':"<all> - shows various informations about akill database"}),
			('add', {
			'permission':'A',
			'callback':self.cmd_add,
			'usage':"<expiry> <user or host> <reason> - Akills a user"}),
			('del', {
			'permission':'A',
			'callback':self.cmd_del,
			'usage':"<id or host> - Removes an akill"}),
		)

	def getHooks(self):
		return (
			('219', self.akill_219),
			('216', self.akill_216),
			('kline', self.akill_kline),
			('unkline', self.akill_unkline),
		)
