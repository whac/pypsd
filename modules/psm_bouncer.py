from psmodule import *
from netaddr import all_matching_cidrs
from untwisted.istring import istring

class PSModule_bouncer(PSModule):
    """Keeps out unwanted connections based on CIDR blacklists"""
    
    VERSION = 1
    
    uid = ""
    klinechan = ""
    
    def startup(self):
        if not PSModule.startup(self): return False
        self.log = logging.getLogger(__name__)
        
        confname = 'bouncer'
        self.blacklist = []

        self.reload()
        
        try:
            self.dbp.execute("CREATE TABLE IF NOT EXISTS cidrbl (id SMALLINT(5) NOT NULL AUTO_INCREMENT, cidr VARCHAR(255), found INT(10), PRIMARY KEY (id), UNIQUE KEY (cidr));")
        except Exception, err:
            self.log.exception("Error creating tables for %s module (%s)" % (confname, err))
        
        try:
            self.dbp.execute("SELECT cidr FROM cidrbl;")
            for row in self.dbp.fetchall(): self.blacklist.append(row[0])
        except Exception, err:
            self.log.exception("Error loading blacklists from DB for %s module (%s)" % (confname, err))
        
        try:
            self.klinechan = istring(self.config.get(confname, 'kline_chan'))
            self.log.debug("Set kline channel to: %s" % self.klinechan)
        except Exception, err:
            self.log.exception("Error reading %s config.ini settings: %s" % (confname, err))
            raise
        
        try:
            self.uid, user = self.parent.createFakeUser(
                self.config.get(confname, 'nick'),
                self.config.get(confname, 'user'),
                self.config.get(confname, 'host'),
                self.config.get(confname, 'gecos'),
                self.config.get(confname, 'modes'),
                self.config.get(confname, 'nspass'),
                version="PyPsd CIDR Blacklist Checker v%d" % self.VERSION,
                join_chans=[self.klinechan,]
            )
        except Exception, err:
            self.log.exception("Error creating %s module user: %s" % (confname, err))
            raise

        return True
    
    def shutdown(self):
        PSModule.shutdown(self)

        self.parent.quitFakeUser(self.uid)

## Begin event hooks
    def bouncer_UID(self, prefix, params):
        """Takes a new user and checks their ip against a CIDR blacklist
        Designed to function for both ipv4 and ipv6 addresses
        
        """
        
        found_addr = all_matching_cidrs(params[6], self.blacklist)
        if found_addr:
            try:
                cidr = istring(found_addr[0]) # first matching is all i care about
                self.dbp.execute(u"UPDATE cidrbl SET found=found+1 WHERE cidr=%s;", (cidr,))
            except Exception, err:
                self.log.exception("Error updating cidrbl found count: %s (%s)" % (cidr, err))
            
            try:
                self.dbp.execute(u"SELECT found FROM cidrbl WHERE cidr=%s;", (cidr,))
                found_num = self.dbp.fetchone()[0]
            except Exception, err:
                self.log.exception("Error seeking cidrbl found count: %s (%s)" % (cidr, err))
                
            if self.parent.get_user("GeoServ") == (None,None): serv = "OperServ"
            else: serv = "GeoServ"
            self.parent.privMsg(serv,
                    "AKILL ADD +3d *@%s Blacklisted CIDR Block - see http://kline.rizon.net/" %
                    (params[6],), self.uid)
            self.parent.privMsg(self.logchan,
                    "CIDR: Banned %s!%s@%s - Blacklisted CIDR: %s (#%d for this block)" %
                    (params[0], params[4], params[6], cidr, found_num), self.uid)
            


## Begin Command hooks
    def cmd_bouncerAdd(self, source, target, pieces):
        if not pieces: return False
        if len(pieces) > 1:
            extra = pieces[1]
        else:
            extra = ""

        to_kill = []
        cidr = pieces[0]

        # do a check to make sure this isn't insane
        try:
            slash = cidr.find('/')+1
            if int(cidr[slash:]) < 16:
                self.parent.privMsg(target, "Sorry, you are not allowed to enter CIDRs with masks lower than 16", self.uid)
                return True
        except:
            self.parent.privMsg(target, "Badly formed CIDR, try again", self.uid)
	    return True

	#who's in the deathpath?
        for uid, user in self.parent.user_t.iteritems():
            if all_matching_cidrs(user['ip'], [cidr]):
                to_kill.append(user)

        if extra == "test":
            self.parent.privMsg(target, "%d hosts would be affected by %s" % (len(to_kill), cidr), self.uid)
	    return True

        if len(to_kill) > 1024:
            self.parent.privMsg(target, "That will affect %d hosts.  Sorry, make it smaller (no override allowed)" % (len(to_kill),), self.uid)
	    return True

        if len(to_kill) > 256 and extra != "override":
            self.parent.privMsg(target, "Warning: %s affects %d hosts.  Append 'override' to continue" % (cidr, len(to_kill)))
	    return True

        else: # enter cidr and kill hosts
            try:
                self.dbp.execute(u"INSERT INTO cidrbl (cidr,found) VALUES(%s,0);", (pieces[0],))
                self.parent.privMsg(target, "Added CIDR blacklist #%d (%d affected)" % (self.dbp.lastrowid, len(to_kill)), self.uid)
            except Exception, err:
                self.log.exception("Error adding version to CIDR module blacklist: (%s)" % err)
                return False

            if self.parent.get_user("GeoServ") == (None,None): serv = "OperServ"
            else: serv = "GeoServ"
            
            killed = 0
            for user in to_kill:
                killed += 1 
                self.parent.privMsg(serv,
                        "AKILL ADD +3d *@%s Blacklisted CIDR Block - see http://kline.rizon.net/" %
                        (user['ip'],), self.uid)
                self.parent.privMsg(self.logchan,
                        "CIDR: Banned %s!%s@%s - Blacklisted CIDR: %s (#%d for this block)" %
                        (user['nick'], user['user'], user['ip'], cidr, killed), self.uid)       
        
            try:
                self.dbp.execute(u"UPDATE cidrbl SET found=found+%d WHERE cidr=%%s;" % (killed,), (cidr,))
            except Exception, err:
                self.log.exception("Error updating cidrbl found count: %s (%s)" % (cidr, err))
    
            self.blacklist.append(pieces[0])        
        
        return True


    def cmd_bouncerDel(self, source, target, pieces):
        if not pieces: return False
        try:
            num = self.dbp.execute("SELECT cidr FROM cidrbl WHERE id=%s", (pieces[0],))
            if not num: raise Exception("No such ID")
            else: cidr = self.dbp.fetchone()[0]

            self.dbp.execute("DELETE FROM cidrbl WHERE id=%s;", (pieces[0],))
            self.blacklist.remove(cidr)
            self.parent.privMsg(target, "Removed blacklist for: %s" % cidr, self.uid)
        except Exception, err:
            self.log.exception("Error deleting CIDR blacklist for CTCP module: (%s)" % err)
            self.parent.privMsg(target, "No such ID", self.uid)

        return True

    def cmd_bouncerList(self, source, target, pieces):
        try:
            self.dbp.execute("SELECT id, cidr, found FROM cidrbl;")
            for row in self.dbp.fetchall():
                self.parent.privMsg(target, "#%d(%s): %s" % (row[0], row[2], row[1]), self.uid)
            self.log.debug("CIDRMEMORY: %s" % self.blacklist)
        except Exception, err:
            self.log.exception("Error selecting CIDR blacklists for bouncer module: (%s)" % err)
        
        self.parent.privMsg(target, "END OF CIDRLIST", self.uid)
        return True

## End Command hooks

    def getCommands(self):
        return (('cidradd', {
            'permission':'c',	
            'callback':self.cmd_bouncerAdd,
            'usage':"<cidr string> [test|override]- blacklists a CIDR block (please use the test parameter first!)"}),
            ('cidrdel', {
            'permission':'c',	
            'callback':self.cmd_bouncerDel,
            'usage':"<cidr id> - removes a blacklisted CIDR block (use cidrlist to find id)"}),
            ('cidrlist', {
            'permission':'c',	
            'callback':self.cmd_bouncerList,
            'usage':"- shows all active blacklisted CIDR blocks"}),
        )
    
    def getHooks(self):
        return (('uid', self.bouncer_UID),
        )
