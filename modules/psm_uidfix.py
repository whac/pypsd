from psmodule import *
class PSModule_uidfix(PSModule):
        VERSION = 1
        
        def startup(self):
                if not PSModule.startup(self): return False
                self.log = logging.getLogger(__name__)
                
                return True

        def cmd_check(self, source, target, pieces):
                """Changes a modules uid in case something got busted
                DO NOT USE UNLESS YOU KNOW EXACTLY WHAT YOU ARE DOING
                """
                
                if not pieces or len(pieces) < 1: return False

                mod = pieces[0]

                try:
                        self.parent.privMsg(target, "UID for %s is %s" % (mod,self.parent.modules[mod].uid))
                except:
                        self.parent.privMsg(target, "Module not loaded or does not have a UID")

                return True

        def cmd_fix(self, source, target, pieces):
                """Changes a modules uid in case something got busted
                DO NOT USE UNLESS YOU KNOW EXACTLY WHAT YOU ARE DOING
                """
                
                if not pieces or len(pieces) < 2: return False

                mod = pieces[0]
                uid = pieces[1]             

                self.parent.modules[mod].uid = uid

                self.parent.privMsg(target, "UID adjusted to %s for %s" % (uid, mod))
                return True

        def getCommands(self):
                return (
                        ('check', {
                        'permission':'N',
                        'callback':self.cmd_check,
                        'usage':"<module> - returns the UID of a module"}),
                        ('fix', {
                        'permission':'N',
                        'callback':self.cmd_fix,
                        'usage':"<module> <new uid> - adjusts a modules internal uid in case something bad happened"}),
                )
