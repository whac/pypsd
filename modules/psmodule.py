#!/usr/bin/python pseudoserver.py
#psmodule.py
# module base
# written by radicand

import logging
from untwisted.istring import istring

class PSModule(object):
	VERSION = 1
	
	parent = None
	logchan = ""
	log = None
	config = None
	dbp = None
	debug = 0
	use_uid = True
	
	def __init__(self, parent, config):
		self.parent = parent
		self.use_uid = self.parent.use_uid
		self.config = config
		self.logchan = istring(config.get('control', 'channel'))
		self.log = logging.getLogger(__name__)
		self.dbp = parent.dbx.cursor()
		self.debug = config.getint('control', 'debug')
		self.started = False
		
	def reload(self):
		self.config.read("config.ini")
		self.debug = self.config.getint('control', 'debug')
	
	def startup(self):
		if self.started: return False
		self.started = True
		return True
	
	def shutdown(self):
		if not self.started: return
	
	def getVersion(self):
		return self.VERSION

	def getCommands(self):
		return ()
			
	def getHooks(self):
		return ()
		
