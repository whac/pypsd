from datetime import datetime
from utils import *
from untwisted.istring import istring

class InvariantCollection(object):
	def __init__(self):
		self.__entries = {}

	def __len__(self):
		return len(self.__entries)

	def __getitem__(self, key):
		invariant_key = istring(key)

		if not self.__contains__(invariant_key):
			raise KeyError('Entry with key %s not present.' % key)

		return self.__entries[invariant_key]

	def __setitem__(self, key, value):
		self.__entries[istring(key)] = value

	def __delitem__(self, key):
		invariant_key = istring(key)

		if not self.__contains__(invariant_key):
			raise KeyError('Entry with key %s not present.' % key)

		del(self.__entries[invariant_key])

	def __iter__(self):
		for entry in self.__entries:
			yield entry

	def __contains__(self, item):
		if not isinstance(item, basestring):
			raise TypeError('%s is not an allowed indexing type.' % type(item))

		return istring(item) in self.__entries
	
	def list_all(self):
		return [self[item] for item in self]

MASK_REGISTERED = create_mask(0, 1)
MASK_BANNED = create_mask(1, 1)
MASK_MASS = create_mask(2, 1)

class CollectionEntity(object):
	def __init__(self, name, flags, ban_source = None, ban_reason = None, ban_date = None, ban_expiry = None):
		self.name = name
		self.flags = flags
		self.ban_source = ban_source
		self.ban_reason = ban_reason
		self.ban_date = ban_date
		self.ban_expiry = ban_expiry
		self.dirty = False

	def clear(self):
		pass

	def get_registered(self):
		return unmask(self.flags, MASK_REGISTERED)

	def set_registered(self, value):
		self.flags = mask(self.flags, MASK_REGISTERED, value)

	def get_banned(self):
		return unmask(self.flags, MASK_BANNED)

	def set_banned(self, value):
		self.flags = mask(self.flags, MASK_BANNED, value)

	def get_mass(self):
		return unmask(self.flags, MASK_MASS)

	def set_mass(self, value):
		self.flags = mask(self.flags, MASK_MASS, value)

	registered = property(get_registered, set_registered)
	banned = property(get_banned, set_banned)
	mass = property(get_mass, set_mass)

class CollectionManager(InvariantCollection):
	def __init__(self, type):
		InvariantCollection.__init__(self)
		self.__type = type
		self.__deleted_items = []

	def commit(self):
		pass

	def check(self, item):
		if not item in self:
			return

		entity = self[item]

		if entity.ban_expiry != None and entity.ban_expiry <= unix_time(datetime.now()):
			self.unban(item)

	def is_dirty(self, item):
		self.check(item)

		return item in self and self[item].dirty

	def is_valid(self, item):
		self.check(item)

		return item in self and self[item].registered and not self[item].banned

	def is_banned(self, item):
		self.check(item)

		return item in self and self[item].banned

	def get(self, item, attribute):
		if not item in self:
			return None

		return getattr(self[item], attribute)

	def set(self, item, attribute, value):
		entity = self.add(item)
		old_value = getattr(entity, attribute)

		if old_value != value:
			setattr(entity, attribute, value)
			entity.dirty = True

	def add(self, item, registered = True):
		if not item in self:
			entity = self.__type(item, 1 if registered else 0)
			entity.dirty = True
			self[item] = entity

		self.on_added(item)

		return self[item]

	def remove(self, item):
		if not item in self:
			return

		entity = self[item]

		if entity.banned and entity.registered:
			entity.registered = False
			entity.clear()
			entity.dirty = True
		elif not entity.banned:
			if not item.lower() in self.__deleted_items:
				self.__deleted_items.append(item.lower())

			del(self[item])

		self.on_removed(item)

	def ban(self, item, source, reason, date, expiry):
		entity = self.add(item, False)
		entity.banned = True
		entity.ban_source = source
		entity.ban_reason = reason
		entity.ban_date = date
		entity.ban_expiry = expiry
		entity.dirty = True

		self.on_banned(item)

	def unban(self, item):
		if not item in self or not self[item].banned:
			return

		entity = self[item]
		entity.banned = False
		entity.ban_source = None
		entity.ban_reason = None
		entity.ban_date = None
		entity.ban_expiry = None

		if not entity.registered:
			self.remove(item)
		else:
			entity.dirty = True

		self.on_unbanned(item)

	def on_added(self, item):
		pass

	def on_removed(self, item):
		pass

	def on_banned(self, item):
		pass

	def on_unbanned(self, item):
		pass

	def clear_deleted(self):
		self.__deleted_items = []

	def list_deleted(self):
		return self.__deleted_items

	def list_dirty(self):
		items = [item for item in self]
		return [self[item] for item in items if self.is_dirty(item)]

	def list_valid(self):
		items = [item for item in self]
		return [self[item] for item in items if self.is_valid(item)]

	def list_banned(self):
		items = [item for item in self]
		return [self[item] for item in items if self.is_banned(item)]
