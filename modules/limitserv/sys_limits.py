import threading
import time
from datetime import datetime
from collections import deque

class LimitManager(object):
	def __init__(self, module):
		self.module = module
		self._queue = deque()
		self._lock = threading.Lock()
		self._delay = self.module.options.get('delay', int, 60)
		self._monitor = None
	
	def insert(self, channel):
		"""Someone joined or left a channel, wait DELAY seconds then fix the limit.
		"""
		with self._lock:
			self.module.elog.debug('Added: %s' % channel)
			self._queue.append({'channel': channel, 'time': datetime.now()})
	
	def queue_monitor(self):
		"""Loops over the queue every second to check for channels to be updated.
		"""
		while self._alive:
			time.sleep(1)
			with self._lock:
				while self._queue and (datetime.now() - self._queue[0]['time']).seconds >= self._delay:
					element = self._queue.popleft()
					self.update_channel(element['channel'])
	
	def start(self):
		"""Starts the queue monitor.
		"""
		self._alive = True
		self._monitor = threading.Thread(target=self.queue_monitor)
		self._monitor.start()
		
	def stop(self):
		"""Stops the queue monitor.
		"""
		self._alive = False
	
	def set_delay(self, new_delay):
		"""Changes interval between channel checks.
		"""
		if self._delay == new_delay:
			return
		
		self.stop()
		self._delay = new_delay
		self.module.options.set('delay', new_delay)
		self.start()
		
	def update_channel(self, channel):
		"""Updates the limit for a channel, 
		the new limit depends on the amount of users.
		"""
		cname, chan = self.module.parent.get_channel(channel)
		if not chan or not self.module.channels.is_valid(cname):
			return
		
		old_limit = chan.get('limit', None)
		users = len(chan.get('members', []))
		
		if users < 100:
			new_limit = users + 5
		elif users < 300:
			new_limit = users + 8
		elif users < 500:
			new_limit = users + 10
		else:
			new_limit = users + 12
		
		if new_limit == old_limit or abs(new_limit - old_limit) < 2: #change mode only if the difference is >= 2
			return
		
		self.module.parent.sendMessage("MODE", cname, "+l %d" % new_limit, prefix=self.module.uid)
		self.module.parent.changeCmodes(cname, "+l", "%d" % new_limit)
		self.module.elog.command('%(chan)s [%(users)d users] Changed limit %(old_limit)d => %(new_limit)d' % {
				'chan': cname,
				'users': users,
				'old_limit': old_limit,
				'new_limit': new_limit})
	
	def resync(self):
		""""Sets correct limits for all channels we joined,
		only used when the bot is loaded.
		"""
		with self._lock:
			for channel in self.module.channels.list_valid():
				self.update_channel(channel.name)
	
	def __contains__(self, channel):
		"""Checks if the channel is already queued for updating.
		"""
		with self._lock:
			for element in self._queue:
				if element['channel'].lower() == channel.lower():
					return True
				
			return False
	
	def __len__(self):
		with self._lock:
			return len(self._queue)
