from datetime import datetime
from decimal import Decimal
from operator import attrgetter, itemgetter

import utils
from cmd_manager import CommandManager

def cmd_activate_vhost(self, manager, channel, sender, arg):
	if arg:
		if ' ' in arg:
			for a in arg.split(' '):
				self.requests.approve(a, sender)
		else:
			self.requests.approve(arg, sender)
	else:
		self.msg(self.chan, 'Syntax: @b%sact {@unick@u | @urequest_id@u}@b' % manager.get_prefix())

def cmd_activate_all(self, manager, channel, sender, arg):
	for nick, req in self.requests.list.items():
		self.requests.approve(req['nick'], sender)

def cmd_except(self, manager, channel, sender, arg):
	if arg:
		if ' ' in arg:
			for r in arg.split(' '):
				self.requests.reject(r, sender)
		else:
			self.requests.reject(arg, sender)
		
		for nick, req in self.requests.list.items():
			self.requests.approve(req['nick'], sender)
	else:
		self.msg(self.chan, 'Syntax: @b%sexc {@unick@u | @urequest_id@u}@b' % manager.get_prefix())
		
def cmd_ban_user(self, manager, channel, sender, arg):
	reason = None
	nick = arg
	if ' ' in arg:
		sp = arg.split(' ')
		nick = sp[0]
		reason = ' '.join(sp[1:])
	
	if nick:
		self.requests.ban(nick, sender, reason)
	else:
		self.msg(self.chan, 'Syntax: @b%sban @unick@u [@ureason@u]@b' % manager.get_prefix())

def cmd_unban_user(self, manager, channel, sender, arg):
	if arg:
		self.requests.unban(arg)
	else:
		self.msg(self.chan, 'Syntax: @b%sunban {@unick@u | @uban_id@u}@b' % manager.get_prefix())

def cmd_ban_list(self, manager, channel, sender, arg):
	if self.requests.banlist:
		if arg:
			blist = [ban for ban in self.requests.banlist.itervalues() if ban.nick.lower() == arg.lower()]
			if not blist:
				self.msg(self.chan, '@b[bans]@b Nick @b%s@b is not banned.' % arg)
				return
		else:
			self.notice(sender, '@b[bans]@b Total %d entries:' % len(self.requests.banlist))
			blist = self.requests.banlist.values()
		
		for n, ban in enumerate(sorted(blist, key=attrgetter('date'))):
			ago = utils.get_timespan(ban.date)
			self.notice(sender, '@b%(num)d.@b @bNick@b %(nick)s @sep @bBanned by@b %(by)s %(ago)s ago @sep%(reason)s' % {
				'num'   : n + 1,
				'nick'  : ban.nick,
				'by'    : ban.banned_by,
				'reason': ' @bReason@b %s @sep' % ban.reason if ban.reason else '',
				'ago'   : ago})
	else:
		self.msg(self.chan, '@b[bans]@b No banned nicks.')

def cmd_blacklist(self, manager, channel, sender, arg):
	type, sep, args = arg.partition(' ')
	if not sep and arg != 'list':
		self.msg(self.chan, 'Syntax: @b%sblacklist <add|del|list> [@uvhost-pattern@u]@b' % manager.get_prefix())
		return
	
	type = type.lower()
	if type == 'add':
		vhost, sep, reason = args.partition(' ')
		self.requests.add_blacklist(vhost, sender, reason if reason else None)
	elif type == 'list':
		foo, bar, match = type.partition(' ')
		if self.requests.blacklist:
			self.notice(sender, '@b[blacklist]@b %d entries:' % len(self.requests.blacklist))
			for n, b in enumerate(self.requests.blacklist):
				self.notice(sender, '@b%(num)d.@b %(vhost)s @sep @bAdded by@b %(by)s %(ago)s ago @sep @bReason@b %(reason)s @sep' % {
					'num': n + 1,
					'ago': utils.get_timespan(b.date),
					'vhost': b.vhost,
					'by': b.added_by,
					'reason': b.reason if b.reason else 'No reason'})
		else:
			self.msg(self.chan, '@b[blacklist]@b No blacklisted vHosts.')
	elif type == 'del':
		vhost, sep, bla = args.partition(' ')
		self.requests.del_blacklist(vhost, sender)
	else:
		self.msg(self.chan, 'Syntax: @b%sblacklist <add|del|list> [@uvhost-pattern@u]@b' % manager.get_prefix())

def cmd_del(self, manager, channel, sender, arg):
	if not arg:
		self.msg(self.chan, 'Syntax: @b%sdel @unick@u@b' % manager.get_prefix())
		return

	nick = arg.split(' ')[0]
	self.msg('HostServ', 'DEL %s' % nick)
	self.msg(self.chan, 'vHost for %s has been removed.' % nick)

def cmd_delall(self, manager, channel, sender, arg):
	if not arg:
		self.msg(self.chan, 'Syntax: @b%sdelall @unick@u@b' % manager.get_prefix())
		return

	nick = arg.split(' ')[0]
	self.msg('HostServ', 'DELALL %s' % nick)
	self.msg(self.chan, 'vHosts for group of %s have been removed.' % nick)

def cmd_list_vhosts(self, manager, channel, sender, arg):
	if arg:
		self.requests.list_vhosts(arg)
	else:
		self.msg(self.chan, 'Syntax: @b%slist @unick@u@b' % manager.get_prefix())
	
def cmd_check_pending(self, manager, channel, sender, arg):
	if self.requests.list:
		s = sorted(self.requests.list.values(), key=itemgetter('id'))
		size = len(self.requests.list)
		self.msg(self.chan, '@b[pending]@b %d pending vHost%s:' % (size, 's' if size != 1 else ''))
		for req in s:
			min = (datetime.now() - req['date']).seconds / 60
			self.msg(self.chan, utils.format_last_req(req['last'], '@b%(id)d.@b %(nick)s @sep @bvHost@b %(vhost)s%(susp)s @sep @bWaiting for@b %(wait)s minute%(s)s @sep%(lastreq)s' % {
				'id'     : req['id'],
				'nick'   : req['nick'],
				'vhost'  : req['vhost'] if not req['suspicious'] else '@c10@b[!]@o ' + req['vhost'],
				'susp'   : ' @sep @bSuspicious@b %s' % req['suspicious'] if req['suspicious'] else '',
				'lastreq': ' @bLast request@b %s ago as %s @sep' % (utils.get_timespan(req['last'].date), req['last'].nickname) if req['last'] else '',
				'wait'   : min,
				's'      : 's' if min != 1 else ''}))
	else:
		self.msg(self.chan, '@b[pending]@b There are no pending vHost requests.')

def cmd_reject_vhost(self, manager, channel, sender, arg):
	if arg:
		remove, foo, reason = arg.partition(' ')
		self.requests.reject(remove, sender, reason)
	else:
		self.msg(self.chan, 'Syntax: @b%srej @unick@u|@urequest_id@u [@ureason@u]@b' % manager.get_prefix())

def cmd_reject_all(self, manager, channel, sender, arg):
	for nick, req in self.requests.list.items():
		self.requests.reject(nick, sender)

def cmd_search_vhost(self, manager, channel, sender, arg):
	if arg:
		self.requests.search(arg)
	else:
		self.msg(self.chan, 'Syntax: @b%ssearch @uvHost@u@b' % manager.get_prefix())

def cmd_stats(self, manager, channel, sender, arg):
	reqs = self.requests.requests
	total = len(reqs)
	topten = {}
	totalwait = 0
	startdate = reqs[0].date
	for req in reqs:
		by = req.resolved_by
		totalwait += req.waited_for
		if by not in topten:
			topten[by] = 1
		else:
			topten[by] += 1
	
	sorted_list = sorted(reqs, key=lambda x: x.waited_for)
	
	days = (datetime.now() - startdate).days
	self.notice(sender, '@sep Approved %(total)s vHosts in %(days)d days @sep Average of %(dailyavg)s approved vHosts/day, one every %(avgappr)s minutes @sep Average wait time %(avgwait)s minutes @sep Median wait time %(median)s minutes @sep' % {
		'total'   : total,
		'days'    : days,
		'dailyavg': round(total / Decimal(days), 2),
		'avgappr' : round(days * Decimal('1440') / total, 2),
		'avgwait' : round(totalwait / (Decimal('60') * total), 2),
		'median'  : round((sorted_list[total / 2].waited_for if total % 2 == 1 else (sorted_list[total / 2].waited_for + sorted_list[total / 2 - 1].waited_for) / 2) / Decimal('60'), 2)})
	
	longest = max([len(u) for u in topten.iterkeys()])
	topten = sorted(topten.iteritems(), key=itemgetter(1), reverse=True)
	if arg != 'all':
		topten = topten[:10]
		
	for n, u in enumerate(topten):
		user, amount = u
		self.notice(sender, '@b#%(num)d@b %(nick)s %(spacer)s @sep @bApproved@b %(amount)d vHosts' % {
			'num': n + 1,
			'nick': user,
			'spacer' : ' ' * (longest - len(user)) if n < 9 else ' ' * (longest - len(user) - 1),
			'amount': amount})

def cmd_suspicious(self, manager, channel, sender, arg):
	type, sep, args = arg.partition(' ')
	if not sep and arg != 'list':
		self.msg(self.chan, 'Syntax: @b%ssuspicious <add|del|list> [@uvhost-pattern@u]@b' % manager.get_prefix())
		return
	
	type = type.lower()
	if type == 'add':
		vhost, sep, reason = args.partition(' ')
		self.requests.add_suspicious(vhost, sender, reason if reason else None)
	elif type == 'list':
		foo, bar, match = type.partition(' ')
		if self.requests.suspicious:
			self.notice(sender, '@b[suspicious]@b %d entries:' % len(self.requests.suspicious))
			for n, b in enumerate(self.requests.suspicious):
				self.notice(sender, '@b%(num)d.@b %(vhost)s @sep @bAdded by@b %(by)s %(ago)s ago @sep @bReason@b %(reason)s @sep' % {
					'num': n + 1,
					'ago': utils.get_timespan(b.date),
					'vhost': b.vhost,
					'by': b.added_by,
					'reason': b.reason if b.reason else 'No reason'})
		else:
			self.msg(self.chan, '@b[suspicious]@b No suspicious vHosts.')
	elif type == 'del':
		vhost, sep, bla = args.partition(' ')
		self.requests.del_suspicious(vhost, sender)
	else:
		self.msg(self.chan, 'Syntax: @b%ssuspicious <add|del|list> [@uvhost-pattern@u]@b' % manager.get_prefix())

def cmd_moo_help(self, manager, channel, sender, arg):
	command = arg.lower()

	if command == '':
		message = ['moo: %shelp moo - for moo commands' % manager.get_prefix()]
	elif command == 'moo':
		message = manager.help
	else:
		return

	for line in message:
		self.notice(sender, line)

class UserCommandManager(CommandManager):
	def get_prefix(self):
		return '!'
	
	def get_commands(self):
		return {
			'a'         : 'activate',
			'act'       : 'activate',
			'activate'  : (cmd_activate_vhost, 'Activates requested vHost to @bnick@b.'),
			
			'aa'        : 'actall',
			'actall'    : (cmd_activate_all, 'Activates all pending vHosts'),
			
			'ban'       : (cmd_ban_user, 'Bans @bnick@b from requesting vHosts with an optional @breason@b and deletes all vHosts in that group. @uNote@u: use it on the main nick to ban an entire group.'),
			
			'banlist'   : (cmd_ban_list, 'Lists active bans.'),
			
			'blacklist' : (cmd_blacklist, 'Add/remove/list blacklist entries.'),

			'del'       : (cmd_del, 'Deletes the vHost of @bnick@b.'),
			'delall'    : (cmd_delall, 'Deletes all vHosts in the group of @bnick@b.'),
			
			'e'         : 'except',
			'exc'       : 'except',
			'except'    : (cmd_except, 'Rejects a [list of] vHosts and activates all the other pending requests.'),
			
			'help'      : (cmd_moo_help, 'Shows this help text.'),
						
			'list'      : (cmd_list_vhosts, 'Lists vHosts requests by @bnick@b.'),
			
			'p'         : 'pending',
			'pen'       : 'pending',
			'pending'   : (cmd_check_pending, 'Lists pending vHost requests.'),
			
			'r'         : 'reject',
			'rej'       : 'reject',
			'reject'    : (cmd_reject_vhost, 'Rejects requested vHost to @bnick@b.'),
			
			'ra'        : 'rejall',
			'rejall'    : (cmd_reject_all, 'Rejects all pending vHosts.'),
			
			'search'    : (cmd_search_vhost, 'Lists users who requested @bvHost@b.'),
			
			'stats'     : (cmd_stats, 'Stats on approved vHosts.'),
			
			'suspicious': (cmd_suspicious, 'Add/remove/list suspicious entries.'),
			
			'unban'     : (cmd_unban_user, 'Unbans @bnick@b from requesting vHosts.')
		}

