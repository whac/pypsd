#!/usr/bin/python pseudoserver.py
#psm-acl.py
# module for pypseudoserver
# written by Adam <adam@rizon.net>
# XXX: merge into core, but check flags on command execution and not as currently done

from psmodule import *

class PSModule_acl(PSModule):
	
	def startup(self):
		if not PSModule.startup(self): return False
		self.log = logging.getLogger(__name__)

		try:
			self.dbp.execute("CREATE TABLE IF NOT EXISTS acl (nick VARCHAR(255), flags VARCHAR(255), PRIMARY KEY (nick));")
		except Exception, err:
			self.log.exception("Error creating table for acl module (%s)" % err)
			raise
		
		return True
	
	def applyACL(self, nick, flags):
		uid, user = self.parent.get_user(nick)
		if user and self.parent.has_umode(nick, 'r'):
			user['acl'] = flags
	
	def cmd_aclAdd(self, source, target, pieces):
		if len(pieces) < 2: return False
		
		uid, user = self.parent.get_user(pieces[0])
		if not user:
			self.parent.privMsg(target, "User " + pieces[0] + " is not online")
			return True

		flags = ""

		try:
			self.dbp.execute("SELECT flags FROM acl WHERE `nick` = %s;", pieces[0])
			flags = self.dbp.fetchone()[0]
		except:
			pass

		change = False
		for c in pieces[1]:
			if flags.find(c) == -1:
				change = True
				flags += c

		if change:
			try:
				self.dbp.execute("INSERT INTO acl (nick, flags) VALUES(%s,%s) ON DUPLICATE KEY UPDATE nick=VALUES(nick), flags=VALUES(flags);", (pieces[0], flags))
				self.parent.privMsg(target, "Flags for %s set to %s" % (pieces[0], flags))
				self.applyACL(pieces[0], flags)
			except Exception, err:
				self.log.exception("Error adding flags to %s: (%s)" % (pieces[0], err))
				return False
		else:
			self.parent.privMsg(target, "Flags for %s unchanged" % pieces[0])

		return True

	def cmd_aclDel(self, source, target, pieces):
		if len(pieces) < 2: return False

		flags = ""

		try:
			self.dbp.execute("SELECT flags FROM acl WHERE `nick` = %s;", pieces[0])
			flags = self.dbp.fetchone()[0]
		except Exception:
			pass

		change = False
		for c in pieces[1]:
			if flags.find(c) != -1:
				change = True
				flags = flags.replace(c, "")

		if change:
			try:
				if flags == "":
					self.dbp.execute("DELETE FROM acl WHERE `nick` = %s", pieces[0])
					self.parent.privMsg(target, "Flags for %s unset" % pieces[0])
				else:
					self.dbp.execute("INSERT INTO acl (nick, flags) VALUES(%s,%s) ON DUPLICATE KEY UPDATE nick=VALUES(nick), flags=VALUES(flags);", (pieces[0], flags))
					self.parent.privMsg(target, "Flags for %s set to %s" % (pieces[0], flags))

				self.applyACL(pieces[0], flags)
			except Exception, err:
				self.log.exception("Error removing flags from %s: (%s)" % (pieces[0], err))
				return False
		else:
			self.parent.privMsg(target, "Flags for %s unchaned" % pieces[0])
			
		return True
		
	def cmd_aclList(self, source, target, pieces):
		try:
			self.dbp.execute("SELECT * FROM acl;")
			for row in self.dbp.fetchall():
				#self.parent.privMsg(target, row[0] + ": " + row[1]) spammy!
				self.parent.notice(target, row[0] + ": " + row[1])
		except Exception, err:
			self.log.exception("Error selecting flags for acl module: (%s)" % err)
		
		self.parent.privMsg(target, "END OF ACL")
		return True

	def acl_JOIN(self, prefix, param):
		if prefix and param and param.lower() == self.parent.logchan.lower():
			uid, user = self.parent.get_user(prefix)
			if not user or not self.parent.has_umode(prefix, 'r'):
				return

			try:
				self.dbp.execute("SELECT flags FROM acl WHERE `nick` = %s", user['nick'])
				user['acl'] = self.dbp.fetchone()[0]
				self.log.debug("ACL for %s set to %s" % (user['nick'], user['acl']))
			except:
				pass

	def getCommands(self):
		return (
			('add', {
			'permission' : 'a',
			'callback' : self.cmd_aclAdd,
			'usage' : "<user> <flags> - Add ACL flags to a user"}),
			('del', {
			'permission' : 'a',
			'callback' : self.cmd_aclDel,
			'usage' : "<user> <flags> - Remove ACL flags from a user"}),
			('list', {
			'permission' : 'a',
			'callback' : self.cmd_aclList,
			'usage' : "- Shows all ACL flags for all users"}),
		)

	def getHooks(self):
		return (
			('join', self.acl_JOIN),
		)
