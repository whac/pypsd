#!/usr/bin/python pseudoserver.py
#psm_session.py
# module for pypseudoserver
# written by Adam <adam@rizon.net>

from psmodule import *
from untwisted import task
import re
import string

class PSModule_session(PSModule):
    
    class session(object):
        limit = 0
        host = None
        used = 0
    
    sessions = {}
    
    def startup(self):
        if not PSModule.startup(self): return False
        self.log = logging.getLogger(__name__)

        try:
            self.dbp.execute("CREATE TABLE IF NOT EXISTS sessions (slimit INT(10), host VARCHAR(255), used INT(1), PRIMARY KEY (host));")
        except Exception, err:
            self.log.exception("Error creating table for session module (%s)" % err)
            raise
        
        try:
            self.dbp.execute("SELECT * FROM sessions WHERE `used` = 1")
            for row in self.dbp.fetchall():
                new_session = self.session()
                new_session.limit = int(row[0])
                new_session.host = row[1]
                new_session.used = 1
                
                self.sessions[row[1]] = new_session
        except Exception, err:
            self.log.exception("Error loading sessions from database (%s)" % err)
            raise

        self.updater = task.LoopingCall(self.update_sessions)
        self.updater.start(600, True)
        selfupdate_sessions()

        return True
    
    def shutdown(self):
        self.updater.stop()
    
    def update_sessions(self):
        self.parent.privMsg("OperServ", "EXCEPTION LIST")
        try:
            for host in self.sessions.keys():
                s = self.sessions[host]
                if s.used == 0:
                    del self.sessions[host]
                else:
                    s.used = 0
            self.dbp.execute("DELETE FROM sessions WHERE `used` = 0")
            self.dbp.execute("UPDATE sessions SET `used` = 0")
        except Exception, err:
            self.log.exception("Error marking session table as not used (%s)" % err)
    
    def on_NOTICE(self, prefix, params):
        uid, user = self.parent.get_user(prefix)
        
        if not user or user['nick'] != 'OperServ':
            return True
        
        try:
            data = re.findall(r"\s*[0-9]+\s*([0-9]+)\s*([a-zA-Z0-9-_.]+)", params[1])
            if not data:
                return True
            limit = data[0][0]
            host = data[0][1]
            
            try:
                session = self.sessions[host]
                if int(limit) == int(session.limit):
                    session.used = 1
                    self.dbp.execute("UPDATE sessions SET `used` = 1 WHERE `host` = %s AND `slimit` = %s", (host, limit))
                    return True
            except KeyError:
                pass
            except Exception, ex:
                self.log.exception("Error while checking if session already exists (%s)" % ex)
                return True
            
            new_session = self.session()
            new_session.limit = int(limit)
            new_session.host = host
            new_session.used = 1
            
            self.sessions[host] = new_session
            
            self.dbp.execute("INSERT INTO sessions (slimit, host, used) VALUES (%s, %s, 1)", (limit, host))
        except Exception, err:
            self.log.exception("Error while parsing exception (%s)" % err)
        
        return True
    
    def getHooks(self):
        return (('notice', self.on_NOTICE),)
