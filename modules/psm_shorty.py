#!/usr/bin/python pseudoserver.py
#psm-shorty.py
# module for pypseudoserver
# written by radicand
# doesn't do much by itself

from psmodule import *
from untwisted.istring import istring
import urllib, urllib2
import base64, hmac, sha

class PSModule_shorty(PSModule):
	VERSION = 1
	
	uid = ""
	
	def startup(self):
		if not PSModule.startup(self): return False
		self.log = logging.getLogger(__name__)

		try:
			self.uid, user = self.parent.createFakeUser(
				self.config.get('shorty', 'nick'),
				self.config.get('shorty', 'user'),
				self.config.get('shorty', 'host'),
				self.config.get('shorty', 'gecos'),
				self.config.get('shorty', 'modes'),
				self.config.get('shorty', 'nspass'),
				version="PyPsd Shorty v%d" % self.VERSION
			)
		except Exception, err:
			self.log.exception("Error creating shorty module user: %s" % err)
			raise

		try:
			self.server = istring(self.config.get('shorty', 'server'))
			self.hmac = istring(self.config.get('shorty', 'hmac'))
			self.email = istring(self.config.get('shorty', 'email'))
		except Exception, err:
			self.log.exception("Error creating shorty module user: %s" % err)
			raise

		return True
	
	def shutdown(self):
		PSModule.shutdown(self)
		
		self.parent.quitFakeUser(self.uid)

	def shortyMakeURL(self, subsys, **params):
		bu = "http://%s/js/%s" % (self.server, subsys)
		ts = self.parent.mkts()
		params['timestamp'] = ts
		pa = [(k,v) for k,v in params.items()]
		pa.sort()
		kv = ['%s=%s' % (urllib.quote(a, ''), urllib.quote(str(b), '')) for a,b in pa]
		ue = '&'.join(kv)
		sb = "GET&%s&%s" % (urllib.quote(bu, ''), urllib.quote(ue, ''))
		s = urllib.quote(base64.b64encode(hmac.new(self.hmac, sb, sha).digest()), '')
		return "%s?%s&oauth_signature=%s" % (bu, ue, s)
		

## Begin Command hooks
	def cmd_shortyPrivmsgAdd(self, source, target, pieces):
		if not pieces: return False
		url = self.shortyMakeURL('get_or_create_shortlink', user=self.email, url=pieces[1], shortcut=pieces[0], is_public="true")
		details = urllib2.urlopen(url)
		self.parent.privMsg(self.logchan, details.read(), self.uid)
		return True

	def cmd_shortyPrivmsgDel(self, source, target, pieces):
		if not pieces: return False
		url = self.shortyMakeURL('get_or_create_shortlink', user=self.email, url="", shortcut=pieces[0], is_public="true")
		details = urllib2.urlopen(url)
		self.parent.privMsg(self.logchan, details.read(), self.uid)
		return True
		
	def cmd_shortyPrivmsgList(self, source, target, pieces):
		self.parent.privMsg(self.logchan, "Unimplemented", self.uid)
		return True
## End Command hooks

	def getCommands(self):
		return (('sladd', {
			'permission':None,
			'callback':self.cmd_shortyPrivmsgAdd,
			'usage':"<shortlink> <dest> - creates a shortlink to dest"}),
			('sldel', {
			'permission':None,
			'callback':self.cmd_shortyPrivmsgDel,
			'usage':"<shortlink> - deletes shortlink"}),
			('sllist', {
			'permission':None,
			'callback':self.cmd_shortyPrivmsgList,
			'usage':"- shows all active shortlinks"}),
		)
